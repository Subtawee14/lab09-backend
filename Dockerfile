FROM openjdk:8-jdk-alpine
EXPOSE 8080

ADD target/lab09-backend.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom",\
    "-jar","/app.jar"]